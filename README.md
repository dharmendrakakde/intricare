## Installation steps
Clone the project and run below commands:

* composer install
* cp .env.example .env

* php artisan key:generate

* php artisan migrate

* php artisan serve
